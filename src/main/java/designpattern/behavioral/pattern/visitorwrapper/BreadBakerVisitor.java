package designpattern.behavioral.pattern.visitorwrapper;

public interface BreadBakerVisitor {
    void visit(MeatEaterBreadBaker meatEaterBreadBaker);
    void visit(VeganBreadBaker veganBreadBaker);
}
