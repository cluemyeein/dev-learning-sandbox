package designpattern.behavioral.pattern.visitorwrapper;

public class CinnamonRaisinBakerVisitor implements BreadBakerVisitor{
    @Override
    public void visit(MeatEaterBreadBaker meatEaterBreadBaker) {
        System.out.println("Baked cinnamon raisin bread for meat eaters.");
    }

    @Override
    public void visit(VeganBreadBaker veganBreadBaker) {
        System.out.println("Baked cinnamon raisin bread for Vegans.");
    }
}
