package designpattern.behavioral.pattern.visitorwrapper;

public interface BreadBakerVisitorWrapper {
    void accept(BreadBakerVisitor visitor);
}
