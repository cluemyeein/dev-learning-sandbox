package designpattern.behavioral.pattern.visitorwrapper;

public interface BreadBaker {
    void orderBread(String order);
    void fulfillOrder(String order);
    boolean bakeMochaCreamBread();
    boolean bakeCheesyBread();
}