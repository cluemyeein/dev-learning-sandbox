package designpattern.behavioral.pattern.visitorwrapper;

public class MeatEaterBreadBaker implements BreadBaker {

    @Override
    public void orderBread(String order) {
        System.out.println("Bread is ordered for meat eaters.");
    }

    @Override
    public void fulfillOrder(String order) {
        System.out.println("Bread order is fulfilled for meat eaters");
    }

    @Override
    public boolean bakeMochaCreamBread() {
        System.out.println("Baked mocha cream bread for meat eaters");
        return true;
    }

    @Override
    public boolean bakeCheesyBread() {
        System.out.println("Baked cheesy bread for meat eaters");
        return true;
    }

}
