package designpattern.behavioral.pattern.visitorwrapper;

public class MeatEaterBreadBakerVisitorWrapper implements BreadBakerVisitorWrapper{
    private final MeatEaterBreadBaker meatEaterBreadBaker;

    public MeatEaterBreadBakerVisitorWrapper(MeatEaterBreadBaker meatEaterBreadBaker) {
        this.meatEaterBreadBaker = meatEaterBreadBaker;
    }

    @Override
    public void accept(BreadBakerVisitor visitor) {
        visitor.visit(meatEaterBreadBaker);
    }
}
