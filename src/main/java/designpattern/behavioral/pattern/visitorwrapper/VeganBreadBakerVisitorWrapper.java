package designpattern.behavioral.pattern.visitorwrapper;

public class VeganBreadBakerVisitorWrapper implements BreadBakerVisitorWrapper{
    private final VeganBreadBaker veganBreadBaker;

    public VeganBreadBakerVisitorWrapper(VeganBreadBaker veganBreadBaker) {
        this.veganBreadBaker = veganBreadBaker;
    }

    @Override
    public void accept(BreadBakerVisitor visitor) {
        visitor.visit(veganBreadBaker);
    }
}
