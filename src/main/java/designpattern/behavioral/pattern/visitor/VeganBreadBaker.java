package designpattern.behavioral.pattern.visitor;

public class VeganBreadBaker implements BreadBaker {
    @Override
    public void orderBread(String order) {
        System.out.println("Bread is ordered for Vegans.");
    }

    @Override
    public void fulfillOrder(String order) {
        System.out.println("Bread is order is fulfilled for Vegans.");
    }

    @Override
    public boolean accept(BreadBakerVisitor visitor) {
        visitor.visit(this);
        return true;
    }
}
