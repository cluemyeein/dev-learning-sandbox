package designpattern.behavioral.pattern.visitor;

public class CheesyBreadBakerVisitor implements BreadBakerVisitor {
    @Override
    public void visit(MeatEaterBreadBaker breadBaker) {
        System.out.println("Cheesy bread baked for meat eaters");
    }

    @Override
    public void visit(VeganBreadBaker breadBaker) {
        System.out.println("Sweet bread baked for Vegans.");
    }
}
