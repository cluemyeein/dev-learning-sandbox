package designpattern.behavioral.pattern.visitor;

public class MochaCreamBreadBakerVisitor implements BreadBakerVisitor {
    @Override
    public void visit(MeatEaterBreadBaker breadBaker) {
        System.out.println("Mocha cream bread is baked for meat eaters");
    }

    @Override
    public void visit(VeganBreadBaker breadBaker) {
        System.out.println("Mocha cream bread is baked for vegans");
    }
}
