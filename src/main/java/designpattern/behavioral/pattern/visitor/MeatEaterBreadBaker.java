package designpattern.behavioral.pattern.visitor;

public class MeatEaterBreadBaker implements BreadBaker {

    @Override
    public void orderBread(String order) {
        System.out.println("Bread is ordered for meat eaters.");
    }

    @Override
    public void fulfillOrder(String order) {
        System.out.println("Bread order is fulfilled for meat eaters");
    }

    @Override
    public boolean accept(BreadBakerVisitor visitor) {
        visitor.visit(this);
        return true;
    }

}
