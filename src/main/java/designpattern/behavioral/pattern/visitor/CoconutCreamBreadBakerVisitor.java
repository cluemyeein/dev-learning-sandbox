package designpattern.behavioral.pattern.visitor;

public class CoconutCreamBreadBakerVisitor implements BreadBakerVisitor {
    @Override
    public void visit(MeatEaterBreadBaker breadBaker) {
        System.out.println("Coconut cream bread baked for meat eaters.");
    }

    @Override
    public void visit(VeganBreadBaker breadBaker) {
        System.out.println("Coconut cream bread baked for vegans.");
    }
}
