package designpattern.behavioral.pattern.visitor;

public interface BreadBakerVisitor {
    void visit(MeatEaterBreadBaker breadBaker);
    void visit(VeganBreadBaker breadBaker);
}
