package designpattern.behavioral.pattern.visitor;

public interface BreadBaker {
    void orderBread(String order);
    void fulfillOrder(String order);
    boolean accept(BreadBakerVisitor visitor);
}