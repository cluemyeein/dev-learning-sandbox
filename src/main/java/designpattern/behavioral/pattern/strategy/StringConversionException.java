package designpattern.behavioral.pattern.strategy;

public class StringConversionException extends Exception {

    public StringConversionException(String message) {
        super(message);
    }

}
