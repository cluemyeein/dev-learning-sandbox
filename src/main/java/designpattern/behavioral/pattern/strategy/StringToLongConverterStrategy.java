package designpattern.behavioral.pattern.strategy;

public class StringToLongConverterStrategy implements StringConverterStrategy {

    @Override
    public ConvertedResult convert(String toConvert) {
        long parsedLong;
        try {
            parsedLong = Long.parseLong(toConvert);
        } catch (NumberFormatException e) {
            return new ConvertedResult(false, e);
        }
        return new ConvertedResult(parsedLong);
    }

}
