package designpattern.behavioral.pattern.strategy;

public class StringConversionExecutor {

    private final StringConverterStrategy strategy;

    public StringConversionExecutor(StringConverterStrategy strategy) {
        this.strategy = strategy;
    }

    public StringConversionExecutor(StringToIntConverterStrategy strategy) {
        this.strategy = strategy;
    }

    public StringConversionExecutor(StringToLongConverterStrategy strategy) {
        this.strategy = strategy;
    }

    public ConvertedResult execute(String toConvert) {
        return strategy.convert(toConvert);
    }

}
