package designpattern.behavioral.pattern.strategy;

public class ConvertedResult {

    private int resultInt;
    private long resultLong;
    private float resultFloat;
    private boolean succeeded = true;
    private Throwable err;

    public ConvertedResult(boolean succeeded, Throwable err) {
        this.succeeded = succeeded;
        this.err = err;
    }

    public ConvertedResult(int resultInt) {
        this.resultInt = resultInt;
    }

    public ConvertedResult(long resultLong) {
        this.resultLong = resultLong;
    }

    public ConvertedResult(float resultFloat) {
        this.resultFloat = resultFloat;
    }

    public int getResultInt() {
        return resultInt;
    }

    public long getResultLong() {
        return resultLong;
    }

    public float getResultFloat() {
        return resultFloat;
    }

    public boolean isSucceeded() {
        return succeeded;
    }

    public Throwable getErr() {
        return err;
    }

    public String getErrorMessage() {
        final String ERROR_PREFIX = "Conversion Failed: ";
        return ERROR_PREFIX + getErr().toString();
    }

}
