package designpattern.behavioral.pattern.strategy;

public class StringToIntConverterStrategy implements StringConverterStrategy {

    @Override
    public ConvertedResult convert(String toConvert) {
        int parsedInt;
        try {
            parsedInt = Integer.parseInt(toConvert);
        } catch (NumberFormatException e) {
            return new ConvertedResult(false, e);
        }
        return new ConvertedResult(parsedInt);
    }

}
