package designpattern.behavioral.pattern.strategy;

public interface StringConverterStrategy {

    ConvertedResult convert(String toConvert);

}
