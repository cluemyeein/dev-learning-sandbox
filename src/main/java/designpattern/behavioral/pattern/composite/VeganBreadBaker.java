package designpattern.behavioral.pattern.composite;

public class VeganBreadBaker implements BreadBaker {
    @Override
    public void orderBread(String order) {
        System.out.println("Bread is ordered for Vegans.");
    }

    @Override
    public void fulfillOrder(String order) {
        System.out.println("Bread is order is fulfilled for Vegans.");
    }

    @Override
    public boolean bakeMochaCreamBread() {
        System.out.println("Baked mocha cream bread for Vegans");
        return true;
    }

    @Override
    public boolean bakeCheesyBread() {
        System.out.println("Baked cheesy bread for Vegans.");
        return true;
    }
}
