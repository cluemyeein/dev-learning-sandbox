package designpattern.behavioral.pattern.composite;

//Element
public interface BreadBaker {
    void orderBread(String order);
    void fulfillOrder(String order);
    boolean bakeMochaCreamBread();
    boolean bakeCheesyBread();
}