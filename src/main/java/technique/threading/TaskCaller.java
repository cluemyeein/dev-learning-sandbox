package technique.threading;

import java.util.concurrent.*;

public class TaskCaller {

    public static void main(String[] args) {

        MyCallableTask callableTask = new MyCallableTask(3, 3);

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Future<Integer> future = executor.submit(callableTask);

        Runnable taskCanceller = () -> {
            System.out.println("The scheduled task is done: " + future.isDone());

            if (!future.isDone()) {

                System.out.println("Attempting to cancel the scheduled task...");
                future.cancel(false);
            }
            try {
                if (future.isDone()) {
                    Integer actualResult = future.get();
                    System.out.println("The future is here with result: " + actualResult);
                }
            } catch (InterruptedException | ExecutionException | CancellationException e) {
                System.out.println(e);
            }

            System.out.println("The scheduled task is cancelled: " + future.isCancelled());
            System.out.println("The scheduled task is done: " + future.isDone());

            executor.shutdown();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        };

        Thread threadOutsideMain = new Thread(taskCanceller);
        threadOutsideMain.start();
    }

}
