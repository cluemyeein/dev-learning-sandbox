package technique.threading;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <p>In this Simple Threading topic, we put into practice on how to use Thread/ExecutorService and Runnable/Callable</p>
 * <p></p>
 * <p>Thread and Runnable</p>
 * <ul>
 *     <li>create a single worker thread by instantiating Thread</li>
 *     <li>create a task by implementing Runnable</li>
 *     <li>run a Runnable task inside a single worker thread</li>
 *     <li>creat a custom thread overriding Run() method that has the task by extending Thread</li>
 *     <li>create more than one thread to demonstrate stopping one thread while the others still continue to run</li>
 *     <li>stop a thread out of a few threads using while-loop and Thread Safe variable <code>volatile</code> as well as <code>AtomicBoolean</code></li>
 * </ul>
 * <p></p>
 * <p>ExecutorService and Callable</p>
 * <ul>
 *     <li>create a task by implementing Callable</li>
 *     <li>run a Callable task using an ExecutorService so to return a Future Boolean</li>
 *     <li>submit a callable task to a thread pool</li>
 * </ul>
 * <p></p>
 * <p>We also covered</p>
 * <ul>
 *     <li>How JVM thread is mapped to System thread, which is by way of JNI native method start0()</li>
 *     <li>The different scenarios to use Runnable vs Callable tasks</li>
 * </ul>
 */
public class SimpleThread {
    public static void main(String[] args) {

        //** create a single worker thread by instantiating Thread, which starts a thread with no task to run. **/
        Thread thread = new Thread();
        thread.start();


        //** create a task and adding some algorithm to the run() method by implementing Runnable **/
        Runnable task = () -> {
            System.out.println("Inside a separate thread, name: " + Thread.currentThread().getName());
            try {
                System.out.println("Time for a quick nap...");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.println("Done napping on thread, name: " + Thread.currentThread().getName());
            }
        };
        //** run a Runnable task inside a single worker thread **/
        Thread taskThread = new Thread(task);
        taskThread.start();

        //** create a task by implementing Callable **/
        Callable<Boolean> taskCB = () -> {
            boolean doneNapping = false;

            System.out.println("Inside a separate thread, name: " + Thread.currentThread().getName());
            try {
                System.out.println("Time for a quick nap...");
                Thread.sleep(5000);
                doneNapping = true;
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                System.out.println("Done napping on thread, name: " + Thread.currentThread().getName());
            }
            return doneNapping;
        };

        //** run a Callable task using an ExecutorService for a thread pool, and also to return a Future Boolean **/
        ExecutorService executor = Executors.newFixedThreadPool(2);
        Future<Boolean> result = executor.submit(taskCB);

        Boolean doneWithMyNap = false;
        try {
            doneWithMyNap = result.get();
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("My nap is over? " + doneWithMyNap);
        }

        //** create more than one thread to demonstrate stopping one thread while the others still continue to run **//
        TaskRunner runner1 = new TaskRunner();
        runner1.start();
        TaskRunner runner2 = new TaskRunner();
        runner2.start();
        TaskRunner runner3 = new TaskRunner();
        runner3.start();

        if (runner1.running.get()) {
            runner1.kill();
        }

    }

    //** creat a custom thread overriding Run() method that has the task by extending Thread **//
    private static class TaskRunner extends Thread {

        AtomicBoolean running = new AtomicBoolean();

        public void kill() {
            running.set(false);
        }

        @Override
        public void run() {
            running.set(true);

            while(running.get()) {
                System.out.println("Inside a separate thread, name: " + Thread.currentThread().getName());
                try {
                    System.out.println("Time for a quick nap...");
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    System.out.println("Done napping on thread, name: " + Thread.currentThread().getName());
                }
            }
        }
    }

}
