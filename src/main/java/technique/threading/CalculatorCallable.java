package technique.threading;

import java.util.concurrent.Callable;

public class CalculatorCallable implements Callable {
    //base, power
    private int base;
    private int power;

    public CalculatorCallable(int base, int power) {
        this.base = base;
        this.power = power;
    }

    @Override
    public Integer call() {
        System.out.println("Calculator is computing...");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
        int result = 1;

        if (base == 0) {
            return 0;
        }
        if (power == 0) {
            return 1;
        }

        for (int i=0; i<power; i++) {
            result = base * result;
        }
        System.out.println("Calculated result is in: " + result);
        return result;
    }
}
