package technique.threading;

import java.util.concurrent.*;

public class CallableCaller {

    public static void main(String[] arg) {

        CalculatorCallable calculatorCallable = new CalculatorCallable(3, 3);
//        try {
//            Integer result = calculatorCallable.call();
//            System.out.println("The answer is: " + result);
//        } catch (Exception exception) {
//            exception.printStackTrace();
//        }

        ExecutorService executor = Executors.newSingleThreadExecutor();
        Future<Integer> futureResult = executor.submit(calculatorCallable);

        Runnable terminator = () -> {
            System.out.println("Terminator is ready to kill...");
            if (!futureResult.isDone()) {
                System.out.println("Attempting to kill the job");
                futureResult.cancel(false);
                executor.shutdown();
            }
            System.out.println("The job is terminated: " + futureResult.isCancelled());
            System.out.println("The job is finished: " + futureResult.isDone());
        };

        Thread killerThread = new Thread(terminator);
        killerThread.start();

        try {
            Integer actualResult = futureResult.get();
            System.out.println("The future is here: " + actualResult);

        } catch (InterruptedException | ExecutionException | CancellationException e) {
            System.out.println(e);
        }
    }
}
