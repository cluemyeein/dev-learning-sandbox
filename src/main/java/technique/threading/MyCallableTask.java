package technique.threading;

import java.util.concurrent.Callable;

public class MyCallableTask implements Callable {

    private int base;
    private int exponent;

    public MyCallableTask(int base, int exponent) {
        this.base = base;
        this.exponent = exponent;
    }

    @Override
    public Integer call() {
        int result = -1;
        try {
//            Thread.sleep(4000);
            System.out.println("threading.MyCallableTask is processing...");

            result = base;
            if (base == 0) {
                return base;
            }
            if (exponent >= 0) {
                Thread.sleep(5000);
                for(int i=1; i<exponent; i++) {
                    result = base * result;
                }
            }

            System.out.println("threading.MyCallableTask has ended. Returning calculated result: " + result);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

}
