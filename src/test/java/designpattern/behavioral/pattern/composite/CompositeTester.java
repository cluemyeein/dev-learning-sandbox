package designpattern.behavioral.pattern.composite;

import org.junit.Test;

public class CompositeTester {

    @Test
    public void test_composite_bake_ops() {
        MeatEaterBreadBaker meatEaterBreadBaker = new MeatEaterBreadBaker();
        VeganBreadBaker veganBreadBaker = new VeganBreadBaker();

        meatEaterBreadBaker.bakeCheesyBread();
        meatEaterBreadBaker.bakeMochaCreamBread();

        veganBreadBaker.bakeCheesyBread();
        veganBreadBaker.bakeMochaCreamBread();
    }
}
