package designpattern.behavioral.pattern.visitorwrapper;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class CinnamonRaisinBakerVisitorTest {

    @Test
    public void test_bake_op_decoupled_by_wrapper() {
        MeatEaterBreadBaker meatEaterBreadBaker = new MeatEaterBreadBaker();
        VeganBreadBaker veganBreadBaker = new VeganBreadBaker();

        System.out.println("Bakers' coupled ops remain the same...");
        meatEaterBreadBaker.bakeMochaCreamBread();
        meatEaterBreadBaker.bakeCheesyBread();
        veganBreadBaker.bakeMochaCreamBread();
        veganBreadBaker.bakeCheesyBread();

        List<BreadBakerVisitorWrapper> visitorWrappers = new ArrayList<>();
        visitorWrappers.add(new MeatEaterBreadBakerVisitorWrapper(meatEaterBreadBaker));
        visitorWrappers.add(new VeganBreadBakerVisitorWrapper(veganBreadBaker));

        System.out.println("Bakers' DECOUPLED ops for baking cinnamon bread...");
        CinnamonRaisinBakerVisitor cinnamonRaisinBakerVisitor = new CinnamonRaisinBakerVisitor();
        for (BreadBakerVisitorWrapper visitorWrapper : visitorWrappers) {
            visitorWrapper.accept(cinnamonRaisinBakerVisitor);
        }
    }
}