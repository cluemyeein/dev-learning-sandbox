package designpattern.behavioral.pattern.strategy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class StringConversionExecutorTest {

    @Test
    public void should_return_converted_value_by_strategy() {

        String testValue = "123";
        ConvertedResult convertedResult = new StringConversionExecutor(new StringToIntConverterStrategy()).execute(testValue);
//        ConvertedResult convertedResult = new StringConversionExecutor(new StringToIntConverterStrategy()).execute(testValue);
        assertNotNull("Should be present", convertedResult);
        assertTrue("Should succeed", convertedResult.isSucceeded());
        assertEquals("Should be converted to the expected value", Integer.parseInt(testValue), convertedResult.getResultInt());
    }

    @Test
    public void should_return_converted_error_by_strategy() {
        String testValue = "gobbadygook";
        ConvertedResult convertedResult = new StringConversionExecutor(new StringToIntConverterStrategy()).execute(testValue);
        assertNotNull("Should be present", convertedResult);
        assertFalse("Should fail", convertedResult.isSucceeded());
        assertFalse("Should have error message", convertedResult.getErrorMessage().isEmpty());
    }

    @Test
    public void should_return_converted_error_by_strategy_long() {
        String testValue = "gobbadygook";
        ConvertedResult convertedResult = new StringConversionExecutor(new StringToLongConverterStrategy()).execute(testValue);
        assertNotNull("Should be present", convertedResult);
        assertFalse("Should fail", convertedResult.isSucceeded());
        assertFalse("Should have error message", convertedResult.getErrorMessage().isEmpty());
    }

}