package designpattern.behavioral.pattern.strategy;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class StringConverterTest {

    @Test
    public void should_Succeed_And_Return_Int() {
        String toConvert = "123";
        StringConverterStrategy converter = new StringToIntConverterStrategy();
        ConvertedResult intResult = converter.convert(toConvert);
        assertNotNull(intResult);
        assertTrue(intResult.isSucceeded());
        assertEquals("Should return an int value", Integer.parseInt(toConvert), intResult.getResultInt());
    }

    @Test
    public void should_Fail_And_Return_Error() {
        String toConvert = "XYZ";
        StringConverterStrategy converter = new StringToIntConverterStrategy();
        ConvertedResult result = converter.convert(toConvert);
        assertNotNull(result);
        assertNotNull(result.getErr());
        assertFalse(result.isSucceeded());
        String expectedErr = result.getErrorMessage();
        System.out.println(expectedErr);
    }

}