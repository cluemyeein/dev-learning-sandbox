package designpattern.behavioral.pattern.visitor;

import org.junit.Test;

public class BreadBakerVisitorTest {
    @Test
    public void test_decoupled_bake_ops() {
        CheesyBreadBakerVisitor cheesyBreadBakerVisitor = new CheesyBreadBakerVisitor();
        CoconutCreamBreadBakerVisitor coconutCreamBreadBakerVisitor = new CoconutCreamBreadBakerVisitor();
        MochaCreamBreadBakerVisitor mochaCreamBreadBakerVisitor = new MochaCreamBreadBakerVisitor();

        MeatEaterBreadBaker meatEaterBreadBaker = new MeatEaterBreadBaker();
        VeganBreadBaker veganBreadBaker = new VeganBreadBaker();

        System.out.println("Order different breads from meat eater baker...");
        cheesyBreadBakerVisitor.visit(meatEaterBreadBaker);
        coconutCreamBreadBakerVisitor.visit(meatEaterBreadBaker);
        mochaCreamBreadBakerVisitor.visit(meatEaterBreadBaker);

        System.out.println("Order different breads from vegan baker...");
        cheesyBreadBakerVisitor.visit(veganBreadBaker);
        coconutCreamBreadBakerVisitor.visit(veganBreadBaker);
        mochaCreamBreadBakerVisitor.visit(veganBreadBaker);
    }
}